import os

class GaussianExtractor(object):
    @staticmethod
    def lazy_extract_from_file(gaussian_output_file):
        """Extrae datos desde el sumario."""
    
        # Leyendo archivo //////////////////////////////////////////
        summary = None
        text = None
        with open(gaussian_output_file) as f:
            text = f.read()
            # Cada elemento del sumario esta en una sola linea
            separator = "\\" if text.find("1|1|")==-1 else "|"
            summary = list(map(lambda l: "".join(map(lambda s: s.strip(),l.strip().splitlines())), text[text.find("1"+separator+"1"+separator):text.find(separator+separator+"@")].split(separator)))

        # Extrayendo datos archivo /////////////////////////////////
        calc = summary[3].lower() # Tipo de cálculo (scan, energy, opt, etc)
        method = summary[4].lower()
        basis = summary[5].lower()
        comment = summary[13] # Comentario del trabajo
        # Cálculo sp ···············································
        if calc == "sp":
            for line in summary:
                # -----------------------------------------------------------------------------
                # DEFINIR TIPOS SOPORTADOS PARA SP
                # -----------------------------------------------------------------------------
                # Obtener el valor de la energía
                # -----------------------------------------------------------------------------

                if method in ["rhf", "uhf","rohf", "ub3lyp", "rb3lyp", "ucam-b3lyp"]:
                    if "HF=" in line: return (calc, method, basis, comment), float(line.split("=")[-1])
                elif method in ["ubd(fc)"]:
                    if "BD=" in line: return (calc, method, basis, comment), float(line.split("=")[-1])
                elif method in ["uccsd-fc", "uccsd-full", "rccsd-fc", "rccsd-full"]:
                    if "CCSD=" in line: return (calc, method, basis, comment), float(line.split("=")[-1])
                elif method in ["ump2-fc"]:
                    if "MP2=" in line: return (calc, method, basis, comment), float(line.split("=")[-1])
                else: 
                    raise ValueError("Método no soportado para SP")
                ###############################################################################
        # Cálculo scan ·············································
        elif calc == "scan":
            # Para obtener nombre de variables desde la tabla
            rows = text[text.find("Summary of the potential surface scan:")+38:text.find("1|1|")]
            rows = rows.strip().splitlines()
            rows = rows[0:1]+rows[2:-1]

            energies = None
            variables = None # Lista con nombre de las variables de scan
            vvalues = dict() # Key: Nombre de variable, Value: Valores de las variables de scan
            
            
            # -----------------------------------------------------------------------------
            # DEFINIR TIPOS SOPORTADOS PARA SCAN
            # -----------------------------------------------------------------------------
            # Se debe dar una lista con los nombres de las variables ordenadas
            # y llenar la lista de energías
            # -----------------------------------------------------------------------------

            if method in ["rhf", "uhf","rohf", "ub3lyp", "rb3lyp", "ucam-b3lyp"]:
                # Obteniendo nombre de variables
                variables = list(map(lambda s: s.strip(), rows[0].strip().split()[1:-1]))

                # Obteniendo valores de energía
                for line in summary:
                    if "HF=" in line:
                        energies = [float(r) for r in map(lambda s: s.strip(),line.split("=")[-1].split(","))]
            elif method in ["ubd(fc)"]:
                # Obteniendo nombre de variables
                variables = list(map(lambda s: s.strip(), rows[0].strip().split()[1:-3]))
                # Obteniendo valores de energía
                for line in summary:
                    if "BD=" in line:
                        energies = [float(r) for r in map(lambda s: s.strip(),line.split("=")[-1].split(","))]
            elif method in ["uccsd-fc", "uccsd-full", "rccsd-fc", "rccsd-full"]:
                # Obteniendo nombre de variables
                variables = list(map(lambda s: s.strip(), rows[0].strip().split()[1:-3]))
                # Obteniendo valores de energía
                for line in summary:
                    if "CCSD=" in line:
                        energies = [float(r) for r in map(lambda s: s.strip(),line.split("=")[-1].split(","))]
            elif method in ["ump2-fc"]:
                # Obteniendo nombre de variables
                variables = list(map(lambda s: s.strip(), rows[0].strip().split()[1:-2]))
                # Obteniendo valores de energía
                for line in summary:
                    if "MP2=" in line:
                        energies = [float(r) for r in map(lambda s: s.strip(),line.split("=")[-1].split(","))]
            else: 
                raise ValueError("Método no soportado para SCAN")
            ###############################################################################
            
            # Preparando listas en el dict para los valores de las variables
            for v in variables: vvalues[v] = list()

            # Obteniendo valores de las variables a partir de sus especificaciones
            for line in summary:
                # Para cada variable se obtiene sus valores (no repetidos)
                for v in variables:
                    if "{0}=".format(v) in line and "s" in line:
                        # Se obtiene un linea como "B1=0.2645886,s,20,0.026459"
                        specs = list(map(lambda s: s.strip(),line.split("=")[-1].split(",")))
                        initial = float(specs[0]) # Valor inicial de la variable de scan
                        size = float(specs[3]) # Tamaño del pas para variable de scan
                        n = int(specs[2]) # Número de pasos
                        # Se rellena el dict con valores para cada variable
                        for i in range(n+1):
                            vvalues[v].append(round(initial+i*size, 6))
                        break
            
            # Calculando el número de puntos a extraer (total combinados)
            nvalues = 1
            for v in vvalues.values(): nvalues *= len(v)

            # Obteniendo el numero de veces que se repite un dato en la tabla
            # La lista ordenada de variables permite calcular este numero
            # haciendo Productos de los números de elementos de las variables previas.

            # La posicion de cada elemento representa a la correspondiente variable
            reps = [0 for i in range(len(variables))] 
            for i in range(len(variables)):
                rep = 1
                k = i
                for j in range(nvalues):
                    while k > 0: 
                        rep *= len(vvalues[variables[k-1]])
                        k -= 1
                # Numero de repeticiones totales de cada dato de cada variable en la tabla
                reps[i] = rep 
            
            # Las repeticiones anteriores sirven para armar los tramos que se repiten
            cicles = [[] for r in reps]

            for vi in range(len(variables)):
                for v in vvalues[variables[vi]]:
                    for ri in range(reps[vi]):
                        cicles[vi].append(v)

            # Añaden los ciclos al diccionario, remplazando los datos previos
            for vi in range(len(variables)):
                vvalues[variables[vi]]= []
                for i in range(nvalues//len(cicles[vi])):
                    vvalues[variables[vi]] += cicles[vi]
            
            # Se comprueba que las listas de resultados tengan mismo tamaño
            for v in vvalues.values(): 
                if len(v) != len(energies): raise RuntimeError("Error al extraer puntos.")

            # Resultados definitivos con cabecera
            results = [tuple(variables + ["E({0}/{1})".format(method, basis)])]
            for i in range(len(energies)):
                varv = []
                for v in variables:
                    varv.append(vvalues[v][i])
                varv.append(energies[i])
                
                results.append(tuple(varv))
                
            return (calc, method, basis, comment), results

    @staticmethod
    def extrac_from_many_files(*gaussian_output_files, **kwargs):
        pass
